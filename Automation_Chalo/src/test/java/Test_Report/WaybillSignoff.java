package Test_Report;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Functions.BaseClass;

public class WaybillSignoff extends BaseClass{
	
	public static  WebDriver driver ;
	public static int sheetnumber = 0;
	//WebDriverWait wait = new WebDriverWait(driver, 15);
	
	//Description: To click on the reports option in the menu
	public void clickReportsInMenu(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		//Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "reports"))));
		 Actions actions = new Actions(driver);
		  actions.moveToElement(driver.findElement(By.xpath(BaseClass.
		  GetPropertyFileData(driver, "reports")))).perform();	
		  System.out.println("The reports button has been clicked");
	}
	
	//Description: To click on the waybill signoff report option
	public void clickWaybillSignoff(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		//Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillSignoff"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillSignoff"))).click();
		System.out.println("The waybill signoff button has been clicked");
	}
	
	//Description: To search the waybill in the waybill signoff reports
	public void searchWaybill(WebDriver driver, String waybill) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "searchWaybill"))).sendKeys(waybill);		 
		//Thread.sleep(1000);
		wait(driver);
		
	}
	
	//Description: To click on the serached waybill
	public void clickWaybill(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		//Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "clickWaybill"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "clickWaybill"))).click();		 
		 
		System.out.println("The waybill has been clicked to check the report");
	}
	
	//Description: To get the total collection in the waybill signoff report
	public String totalCollection(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		//Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "totalCollection"))));
		String TotalCollection = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "totalCollection"))).getText();	 

		System.out.println("The total cash in the report is " + TotalCollection);
		
		return TotalCollection;
		 
	}
	
	//Description: To get the syned collection in the waybill signoff report
	public String collectionSynced(WebDriver driver, int Sheetnumber, int row) throws InterruptedException, FileNotFoundException, IOException {
		//Thread.sleep(1000);
		wait(driver);
		String CollectionSyned= BaseClass.testdata(Sheetnumber, row, 9);
	
		System.out.println("The syned cash is " + CollectionSyned);
		
		return CollectionSyned;
		 
	}

}
