package Test_DailyOps;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Functions.BaseClass;

public class CollectionModule extends BaseClass{

	public static  WebDriver driver ;
	public static int sheetnumber = 0;
	//WebDriverWait wait = new WebDriverWait(driver, 15);
	
	//Description: To click on the daily ops option in the menu
	public void clickDailyOpsOption(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "dailyOps"))));
		 Actions actions = new Actions(driver);
		  actions.moveToElement(driver.findElement(By.xpath(BaseClass.
		  GetPropertyFileData(driver, "dailyOps")))).perform();		
		  
		  System.out.println("The daily ops option has been clicked");
	}
	
	//Description: To click on the collection option in the menu
	public void clickCollectionInMenue(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "collectionButton"))).click();  //To click on collection button in menu
		 wait(driver);
		
		 System.out.println("The button of collection page has been opened and the waybill is synced from the machine");
		 wait(driver);
		 
	}
	
	//Description: To search waybill on the collection page
	public void searchWaybill(WebDriver driver, String waybill) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "searchWaybill"))).sendKeys(waybill);	
		wait(driver);
		
	}
	
	//Description: To enter the total cash collection
	public void enterTotalCashSalesCollection(WebDriver driver, int Sheetnumber, int row) throws InterruptedException, FileNotFoundException, IOException {
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "totalCashSalesCollection"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "totalCashSalesCollection"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 9));
		wait(driver);
		
		System.out.println("The total sales cash collection has been entered");
		
	}
	
	//Description: To click on the submit button of the collection
	public void clickSubmit(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "collectionSubmit"))).click();	
		wait(driver);
		
		System.out.println("The collecton has been submitted");
		wait(driver);
		
	}
	
	//Description: To click the confirmation button
	public void clickYes(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "checkbox"))));
		Thread.sleep(2000);
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "checkbox"))).click();	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "clickYes"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "clickYes"))).click();	
		wait(driver);
		
		System.out.println("The yes button has been clicked");
		wait(driver);
		
	}
}
