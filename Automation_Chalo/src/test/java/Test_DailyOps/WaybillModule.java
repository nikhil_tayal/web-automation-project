package Test_DailyOps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Functions.BaseClass;

public class WaybillModule extends BaseClass{

	public static  WebDriver driver ;
	public static int sheetnumber = 0;
	//WebDriverWait wait = new WebDriverWait(driver, 15);

	//Description: To click on the daily ops option in the menu
	public void clickDailyOpsOption(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "dailyOps"))));
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.xpath(BaseClass.
				GetPropertyFileData(driver, "dailyOps")))).perform();	
		System.out.println("The daily ops button has been clicked");
	}

	//Description: To click on the waybills option in the menu
	public void clickWaybillInMenue(WebDriver driver) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillsButton"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillsButton"))).click();  //To click on waybills button in menu
		wait(driver);
		System.out.println("The add waybill button has been clicked");
	}

	//Description: To create the waybill
	public void createWaybill(WebDriver driver, int Sheetnumber, int row) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 4));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 5));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 6));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 7));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 8));
		
		//if (driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillShift"))).isDisplayed()) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillShift"))));
			driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillShift"))).sendKeys("Morning");
			//driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillShift"))).sendKeys(Keys.ENTER);
		/*}
		else {
			System.out.println("The shift is already selected");
		}*/
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))));
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
		
		wait(driver);

		System.out.println("The waybill has been created successfully");

	}


	//Description: To search the waybill on the waybills page
	public void searchWaybill(WebDriver driver, int sheetnumber, int row, int column) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillSearch"))).sendKeys(BaseClass.testdata(sheetnumber, row, column));		 
		
		wait(driver);
	}

	//Description: To craete the promoter waybill
	public void promoterWaybill(WebDriver driver, int Sheetnumber, int row) throws InterruptedException, FileNotFoundException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterRadioBtn"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterRadioBtn"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterSelect"))));

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterSelect"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterPromoter"))));
		
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterPromoter"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 10));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterPromoter"))));
		
		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterPromoter"))).sendKeys(Keys.ENTER);

		driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "createPromoterWaybill"))).click();
		
		wait(driver);

		System.out.println("The waybill for promoter has been created successfully");

	}

}
