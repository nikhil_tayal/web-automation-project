package Test_Regression;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.BrowserVersion;

import Functions.BaseClass;
import Modules_Pages.LoginPageModule;
import Test_DailyOps.CollectionModule;
import Test_DailyOps.WaybillModule;
import Test_Report.WaybillSignoff;


public class Waybill_Creation extends BaseClass{

	public WebDriver driver;
	LoginPageModule objLoginPage = new LoginPageModule();
	WaybillModule objWaybillPage = new WaybillModule();
	CollectionModule objCollectionPage = new CollectionModule();
	WaybillSignoff objWaybillSignoff = new WaybillSignoff();
	public int i=0;
	//WebDriverWait wait = new WebDriverWait(driver, 15);

	//Description : To login the application
	@Parameters ({"browser","env"})
	@BeforeClass
	public void beforeloginTest( String browser, String env) throws InterruptedException, FileNotFoundException, IOException

		{
					   
		//BaseClass bc= new BaseClass();
		environment(env);
		driver = SetupBrowser(browser);
		wait(driver);
		BrowserMaximize(driver);
		
/*		driver.get("https://testproject.io/?utm_source=google-ads&utm_campaign=website_testing&utm_agid=116246299425&utm_term=web%20test%20automation&creative=492868597094&device=c&placement&gclid=CjwKCAjw87SHBhBiEiwAukSeUfSd0pEPd4La4UDPrvbHo6PYknRugcjzcu5gNP-0-eKnartxLTQqYxoCGp8QAvD_BwE");
		driver.findElement(By.xpath("//*[@id='menu-item-524']/a")).click();
		System.out.println("The forum button has been clicked");
		driver.findElement(By.xpath("//*[@id=\"menu-item-901\"]/a")).click();
		System.out.println("The login button has been clicked");*/
		
		
		if(env.contains("stg"))
		{
		navigateUrl(driver);
		}
		else if(env.contains("PreProd"))
		{
		navigateUrlPreProd(driver);
		}
		else
		{
		navigateProd(driver); 
		}
		wait(driver);
		
		
		System.out.println("Testing Login Scenario Valid...");
		
		LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
		 
		 logs.getAll();
		 
		 objLoginPage.loginEnterDataFields(driver, 0, 1);
		 
		 wait(driver);
		 
		 analyzeLog();
		 
		 objLoginPage.selectCity(driver, 0, 1);
		 
		 wait(driver);
		}
	
	private void setJavascriptEnabled(boolean b) {
		// TODO Auto-generated method stub
		
	}

	public void analyzeLog() {
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
 
        }
    }
	
	
	//Description : To test the waybill creation page and collection valid input
	@Test(enabled=true,priority=1)
	@Parameters ({"browser","env"})
	public void Test1_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
	{
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, 15);
			System.out.println("Testing waybill creation page and collection valid input..."
					+ "\n");
			
			LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
			 
			 logs.getAll();

			 analyzeLog();
			 
			 objWaybillPage.clickDailyOpsOption(driver);
			 wait(driver);
			 
				
			 objWaybillPage.clickWaybillInMenue(driver);
			 wait(driver);
			 
			 
			 objWaybillPage.createWaybill(driver, 0, 1);
			 wait(driver);
			 
				
			 objWaybillPage.searchWaybill(driver, 0, 1, 6);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))));
			 
			 
			 String waybillNo = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))).getText();
			 wait(driver);
			 
			 System.out.println("The created waybill number is: " + waybillNo);		
			 
			 wait(driver);
			 WriteIntoExcelSheet(0 ,0 ,0 , waybillNo );
			 
			 wait(driver);
			 Runtime.getRuntime().exec("python3 script_name.py");
			 System.out.println("The command to execute the database query has been run successfully");
			 
			 objCollectionPage.clickDailyOpsOption(driver);
			 wait(driver);
			 
			 
			 objCollectionPage.clickCollectionInMenue(driver);
			 wait(driver);
			 
			 
			 objCollectionPage.searchWaybill(driver, waybillNo);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "syncStatus"))));
			 
			 
			 String syncButton = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "syncStatus"))).getText();
			 

			 if(syncButton.equalsIgnoreCase("synced")) {
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "syncButton"))).click();
				 System.out.println("The sync button has been clicked successfully");
				 Thread.sleep(1000);
			 }
			 
			 else {
				 System.out.println("The waybill has not been marked as complete successfully");
			 }

			 
			 objCollectionPage.enterTotalCashSalesCollection(driver, 0, 1);
			 wait(driver);
			 
			  
			 objCollectionPage.clickSubmit(driver);
			 wait(driver);
			 
			 
			 objCollectionPage.clickYes(driver);
			 wait(driver);
			
			 
			 objWaybillSignoff.clickReportsInMenu(driver);
			 wait(driver);
			 
			 
			 objWaybillSignoff.clickWaybillSignoff(driver);
			 wait(driver);
			 
			 
			 objWaybillSignoff.searchWaybill(driver, waybillNo);
			 wait(driver);
			 
			 
			 objWaybillSignoff.clickWaybill(driver);
			 wait(driver);
			 
			 
			 String totalCollection = objWaybillSignoff.totalCollection(driver);
			 wait(driver);
			 
			 
			 String collectionSynced = objWaybillSignoff.collectionSynced(driver, 0, 1);
			 wait(driver);
			 
			 
			 if (totalCollection.contains(collectionSynced)) {
				 
				System.out.println("The total collection on the waybill signoff report is correct");
				wait(driver);
				
			 }
			 
			 else {
				 
				System.out.println("The total collection on the waybill signoff report is incorrect");
				wait(driver);
				
			 }
			 
			 
		}catch(Exception e)
		{
			 wait(driver);
			 e.printStackTrace();
			 Assert.fail();
		}
	}
		
	//Description : To test the waybill creation without mandatory fields invalid scenario
	@Test(enabled=true,priority=2)
	@Parameters ({"browser","env"})
	public void Test2_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
	{
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, 15);
			
			System.out.println("Testing waybill creation without mandatory fields invalid scenario.."
					+ "\n");
			
			int Sheetnumber = 0;
			int row = 1;
						
			LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
			 
			 logs.getAll();
			 
			 objWaybillPage.clickDailyOpsOption(driver);
			 wait(driver);
			 
				
			 objWaybillPage.clickWaybillInMenue(driver);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));			 
				
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))));
			 System.out.println("The add waybill button has been clicked");

			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(BaseClass.testdata(0, 1, 4));
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(Keys.ENTER);
			 
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 5));
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(Keys.ENTER);
			 
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 6));
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(Keys.ENTER);
			 
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "toastMessage"))));
			 
			 String toastMessage = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "toastMessage"))).getText();
			 
			 System.out.println("The validation message is "+ toastMessage);
			 
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
			
			driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
			System.out.println("The create waybill popup has been closed");
			
			String result;
			
			if (toastMessage.contains("please enter Route and TripRound")) {
				
				result = "true";
			}
			
			else {
				
				result = "false";
			}
			
			Assert.assertEquals(result, "true");
			
			
			wait(driver);
			
		}
		
		catch(Exception e) 
		{
			wait(driver);
			 
			 e.printStackTrace();
			 Assert.fail();
		}
	}
	
	//Description : To test the waybill creation wrong inputs invalid scenario
		@Test(enabled=true,priority=3)
		@Parameters ({"browser","env"})
		public void Test3_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
		{
			try 
			{
				WebDriverWait wait = new WebDriverWait(driver, 15);
				
				System.out.println("Testing waybill creation wrong inputs invalid scenario.."
						+ "\n");
				
				int Sheetnumber = 0;
				int row = 2;
							
				LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
				 
				 logs.getAll();
				 
				 objWaybillPage.clickDailyOpsOption(driver);
				 
				 wait(driver);
					
				 objWaybillPage.clickWaybillInMenue(driver);
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
				 System.out.println("The add waybill button has been clicked");
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))));

				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))).click();
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(BaseClass.testdata(0, row, 4));
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(Keys.ENTER);
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))).click();
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 5));
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(Keys.ENTER);
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))).click();
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 6));
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(Keys.ENTER);
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))).click();
				 
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 7));
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(Keys.ENTER);
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 8));
				 String toastMessage = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "toastMessage"))).getText();
				 
				 System.out.println("The validation message is "+ toastMessage);
				 
				 
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
				 
				driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
				System.out.println("The create waybill popup has been closed");
				
				String result;
				
				if (toastMessage.contains("please enter Route and TripRound")) {
					
					result = "true";
				}
				
				else {
					
					result = "false";
				}
				
				Assert.assertEquals(result, "true");
				
				
				wait(driver);
				
			}
			
			catch(Exception e) 
			{
				 wait(driver);
				 e.printStackTrace();
				 Assert.fail();
			}
		}
		
		
		//Description : To test the waybill creation without bus number invalid scenario
				@Test(enabled=true,priority=4)
				@Parameters ({"browser","env"})
				public void Test4_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
				{
					try 
					{
						WebDriverWait wait = new WebDriverWait(driver, 15);
						
						System.out.println("Testing waybill creation without bus number invalid scenario.."
								+ "\n");
						
						int Sheetnumber = 0;
						int row = 1;
									
						LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
						 
						 logs.getAll();
						 
						 objWaybillPage.clickDailyOpsOption(driver);
						 
						 wait(driver);
							
						 objWaybillPage.clickWaybillInMenue(driver);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
							
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
						 System.out.println("The add waybill button has been clicked");
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 5));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(Keys.ENTER);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectConductor"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 6));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(Keys.ENTER);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));

						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 7));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(Keys.ENTER);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 8));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
							
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
						 System.out.println("The create waybill popup has been closed");
						 
						 wait(driver);
						
					}
					
					catch(Exception e) 
					{
						wait(driver);
						 
						 e.printStackTrace();
						 Assert.fail();
					}
				}
				
				//Description : To test the waybill creation without driver id invalid scenario
				@Test(enabled=true,priority=5)
				@Parameters ({"browser","env"})
				public void Test5_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
				{
					try 
					{
						WebDriverWait wait = new WebDriverWait(driver, 15);
						
						System.out.println("Testing waybill creation without driver id invalid scenario.."
								+ "\n");
						
						int Sheetnumber = 0;
						int row = 1;
									
						LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
						 
						 logs.getAll();
						 
						 objWaybillPage.clickDailyOpsOption(driver);
						 
						 wait(driver);
						 
						 objWaybillPage.clickWaybillInMenue(driver);
						 
							
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
						 System.out.println("The add waybill button has been clicked");
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(BaseClass.testdata(0, 1, 4));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(Keys.ENTER);
						 
						 
						/* driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 6));*/
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterConductor"))).sendKeys(Keys.ENTER);
						
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 7));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));

						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(Keys.ENTER);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 8));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
							
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
						 System.out.println("The create waybill popup has been closed");
						 
						wait(driver);
					}
					
					catch(Exception e) 
					{
						wait(driver);
						 
						 e.printStackTrace();
						 Assert.fail();
					}
				}
				
				//Description : To test the waybill creation without conductor id invalid scenario
				@Test(enabled=true,priority=6)
				@Parameters ({"browser","env"})
				public void Test6_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
				{
					try 
					{
						WebDriverWait wait = new WebDriverWait(driver, 15);
						
						System.out.println("Testing waybill creation without conductor id invalid scenario.."
								+ "\n");
						
						int Sheetnumber = 0;
						int row = 1;
									
						LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
						 
						 logs.getAll();
						 
						 objWaybillPage.clickDailyOpsOption(driver);
						 
						 wait(driver);
							
						 objWaybillPage.clickWaybillInMenue(driver);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
						
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
						 System.out.println("The add waybill button has been clicked");
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))));

						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectBus"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(BaseClass.testdata(0, 1, 4));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterBus"))).sendKeys(Keys.ENTER);
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectDriver"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 5));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterDriver"))).sendKeys(Keys.ENTER);
						
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "selectRoute"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));

						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 7));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "enterRoute"))).sendKeys(Keys.ENTER);
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "tripRound"))).sendKeys(BaseClass.testdata(Sheetnumber, row, 8));
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))));
						 
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "submitWaybill"))).click();
						 
						 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
							
						 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
						 System.out.println("The create waybill popup has been closed");
						 
						 wait(driver);
						
					}
					
					catch(Exception e) 
					{
						wait(driver);
						 
						 e.printStackTrace();
						 Assert.fail();
					}
				}
	
		//Description : To logout the application 
		@AfterClass
		public void logOutTest() throws IOException, InterruptedException 
			{
				wait(driver);	
				objLoginPage.logOut(driver);
				BaseClass.closeBrowser(driver);
//				driver.close();
				driver.quit();
	}
	
}
