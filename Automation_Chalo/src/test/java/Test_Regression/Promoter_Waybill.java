package Test_Regression;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Functions.BaseClass;
import Modules_Pages.LoginPageModule;
import Test_DailyOps.CollectionModule;
import Test_DailyOps.WaybillModule;
import Test_Report.WaybillSignoff;

public class Promoter_Waybill extends BaseClass{
	
	public WebDriver driver;
	LoginPageModule objLoginPage = new LoginPageModule();
	WaybillModule objWaybillPage = new WaybillModule();
	CollectionModule objCollectionPage = new CollectionModule();
	WaybillSignoff objWaybillSignoff = new WaybillSignoff();
	public int i=0;
	//WebDriverWait wait = new WebDriverWait(driver, 15);

	//Description : To login the application
	@Parameters ({"browser","env"})
	@BeforeClass
	public void beforeloginTest( String browser, String env) throws InterruptedException, FileNotFoundException, IOException

		{
					   
		//BaseClass bc= new BaseClass();
		environment(env);
		driver = SetupBrowser(browser); 
		wait(driver);
		BrowserMaximize(driver);
		if(env.contains("stg"))
		{
		navigateUrl(driver);
		}
		else
		{
		navigateUrlPreProd(driver); 
		}
		wait(driver);
		
		
		System.out.println("Testing Login Scenario Valid...");
		
		LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
		 
		 logs.getAll();
		
		 objLoginPage.loginEnterDataFields(driver, 0, 1);
		 
		 wait(driver);
		 
		 analyzeLog();
		 
		 objLoginPage.selectCity(driver, 0, 1);
		 
		 wait(driver);
		}
	
	public void analyzeLog() {
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
            //do something useful with the data
        }
    }
	
	
	//Description : To test the promoter waybill creation and collection valid input
	@Test(enabled=true,priority=1)
	@Parameters ({"browser","env"})
	public void Test1_WaybillCreationAndCollectionTest(String browser, String env) throws Exception 
	{
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, 15);
			System.out.println("Testing promoter waybill creation and collection valid input..."
					+ "\n");
			
			LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
			 
			 logs.getAll();
			 
			 analyzeLog();
			 
			 objWaybillPage.clickDailyOpsOption(driver);
			 
			 wait(driver);
				
			 objWaybillPage.clickWaybillInMenue(driver);
			 
			 wait(driver);
			 
			 objWaybillPage.promoterWaybill(driver, 0, 1);
			 
			 wait(driver);
				
			 objWaybillPage.searchWaybill(driver, 0, 1, 10);
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))));
			 
			 String waybillNo = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillNumber"))).getText();
			 
			 wait(driver);
			 
			 System.out.println("The created waybill number is: " + waybillNo);
			 
			 wait(driver);
			 WriteIntoExcelSheet(0 ,0 ,0 , waybillNo );
			 
			 wait(driver);
			 Runtime.getRuntime().exec("python3 script_name.py");
			 System.out.println("The command to execute the database query has been run successfully");
			 
			 objCollectionPage.clickDailyOpsOption(driver);
			 
			 wait(driver);
			 
			 objCollectionPage.clickCollectionInMenue(driver);
			 
			 wait(driver);
			 
			 objCollectionPage.searchWaybill(driver, waybillNo);
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "syncStatus"))));
			 
			 String syncButton = driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "syncStatus"))).getText();

			 if(syncButton.equalsIgnoreCase("synced")) {
				 
				 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "syncButton"))).click();
				 System.out.println("The sync button has not been clicked successfully");

			 }
			 
			 else {
				 System.out.println("The waybill has not been marked as complete successfully");
			 }
			 
			 objCollectionPage.enterTotalCashSalesCollection(driver, 0, 1);
			 
			 wait(driver);
			 
			 objCollectionPage.clickSubmit(driver);
			 
			 wait(driver);
			 
			 objCollectionPage.clickYes(driver);
			 
			 wait(driver);
			 
			 objWaybillSignoff.clickReportsInMenu(driver);
			 
			 wait(driver);
			 
			 objWaybillSignoff.clickWaybillSignoff(driver);
			 
			 wait(driver);
			 
			 objWaybillSignoff.searchWaybill(driver, waybillNo);
			 
			 wait(driver);
			 
			 objWaybillSignoff.clickWaybill(driver);
			 
			 wait(driver);
			 
			 String totalCollection = objWaybillSignoff.totalCollection(driver);
			 
			 wait(driver);
			 
			 String collectionSynced = objWaybillSignoff.collectionSynced(driver, 0, 1);
			 
			 wait(driver);
			 
			 if (totalCollection.contains(collectionSynced)) {
				 
				System.out.println("The total collection on the waybill signoff report is correct");
				
				wait(driver);
			 }
			 
			 else {
				 
				System.out.println("The total collection on the waybill signoff report is incorrect");
				
				wait(driver);
			 }
			 
			 
		}catch(Exception e)
		{
			wait(driver);
			 
			 e.printStackTrace();
			 Assert.fail();
		}
	}
	
	//Description : To test the waybill creation without conductor id invalid scenario
	@Test(enabled=true,priority=2)
	@Parameters ({"browser","env"})
	public void Test2_PromotorWaybillCreationAndCollectionTest(String browser, String env) throws Exception 
	{
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, 15);
			
			System.out.println("Testing waybill creation without conductor id invalid scenario.."
					+ "\n");
						
			LogEntries logs = driver.manage().logs().get(LogType.BROWSER);
			 
			 logs.getAll();
			 
			 objWaybillPage.clickDailyOpsOption(driver);
			 
			 wait(driver);
				
			 objWaybillPage.clickWaybillInMenue(driver);
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))));
				
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "addWaybill"))).click();  //To click on add waybills button
			 System.out.println("The add waybill button has been clicked");
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterRadioBtn"))));

			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "promoterRadioBtn"))).click();
			 System.out.println("The promoter radio button has been clicked");
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "createPromoterWaybill"))));
			 
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "createPromoterWaybill"))).click();
			 System.out.println("Clicked on the create waybill button");
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))));
				
			 driver.findElement(By.xpath(BaseClass.GetPropertyFileData(driver, "waybillClose"))).click();
			 System.out.println("The craete waybill popup is close");
			 
			 wait(driver);
			
		}
		
		catch(Exception e) 
		{
			wait(driver);
			 
			 e.printStackTrace();
			 Assert.fail();
		}
	}
	
	//Description : To logout the application 
			@AfterClass
			public void logOutTest() throws IOException, InterruptedException 
				{
					wait(driver);
						
					objLoginPage.logOut(driver);
					BaseClass.closeBrowser(driver);
//					driver.close();
					driver.quit();
		}

}
