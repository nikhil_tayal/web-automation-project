package Functions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.html.HtmlOption;

public class BaseClass {
	public static  WebDriver driver;
	public static int sheetnumber = 0;
	//public static String ElementFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\Resources\\ORStg.properties";
	//public static String ElementFilePath2 = System.getProperty("user.dir")+"\\src\\test\\java\\Resources\\ORPreProd.properties";
	//public static String ElementFilePath3 = System.getProperty("user.dir")+"\\src\\test\\java\\Resources\\ORProd.properties";
	public static String ElementFilePath = System.getProperty("user.dir")+"/src/test/java/Resources/ORStg.properties";
	public static String ElementFilePath2 = System.getProperty("user.dir")+"/src/test/java/Resources/ORPreProd.properties";
	public static String ElementFilePath3 = System.getProperty("user.dir")+"/src/test/java/Resources/ORProd.properties";
	public static FileInputStream fis;
	public static String currenturl;

	public static String  applicationUrl= "http://stagingafcs.chalo.com/home";
	public static String  applicationUrl1= "http://preprod-afcs.chalo.com/home";
	public static String applicationUrl2="https://afcs.chalo.com/"; 
	public static String env;

	//Description: Waits for page load
	//Input= NA
	//Output= NA
	public static void wait(WebDriver driver)
	{

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}
	@SuppressWarnings("static-access")
	public  void environment(String env) 
	{
		this.env=env;
	}

	//Description: Reads data from OR Properties File
	//Input= Properties File
	//Output= locator values of elements

	public static String GetPropertyFileData(WebDriver driver,String label) throws FileNotFoundException, IOException 
	{

		if(env.contains("stg"))
		{
			Properties prop = new Properties(); 
			prop.load(new FileInputStream(ElementFilePath));
			String FeildValue = prop.getProperty(label);
			return FeildValue;
		}
		else if(env.contains("PreProd"))
		{
			Properties prop = new Properties(); 
			prop.load(new FileInputStream(ElementFilePath2));
			String FeildValue = prop.getProperty(label);
			return FeildValue;
		}
		else
		{
			Properties prop = new Properties(); 
			prop.load(new FileInputStream(ElementFilePath3));
			String FeildValue = prop.getProperty(label);
			return FeildValue;
		}
	}

	//Description: Maximizes the Browser
	//Input = NA
	//Output = Maximizes the Browser

	public static void BrowserMaximize(WebDriver driver) throws InterruptedException 
	{
		wait(driver);

		driver.manage().window().maximize();
		wait(driver);

	}

	//Description: To navigate the Application URL
	//Input = Application URL
	//Output = Displays login page of application

	public static void navigateUrl(WebDriver driver  )
			throws FileNotFoundException, IOException, InterruptedException 
	{
		driver.navigate().to(applicationUrl);
		wait(driver);

		currenturl =driver.getCurrentUrl();
	}
	public static void navigateProd(WebDriver driver  )
			throws FileNotFoundException, IOException, InterruptedException 
	{
		driver.navigate().to(applicationUrl2);
		wait(driver);

		currenturl =driver.getCurrentUrl();
	}


	public static void navigateUrlPreProd(WebDriver driver  )
			throws FileNotFoundException, IOException, InterruptedException 
	{
		driver.navigate().to(applicationUrl1);
		wait(driver);

		currenturl =driver.getCurrentUrl();
	}


	public static WebDriver SetupBrowser( String browser ) throws IOException
	{

		if(browser.equalsIgnoreCase("firefox"))
		{

			//System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\geckodriver.exe");
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/geckodriver.exe");
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
			//System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"C:\\temp\\logs.txt");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"C:/temp/logs.txt");
			FirefoxOptions Options = new FirefoxOptions();
			//Options.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); //Location where Firefox is installed
			Options.setBinary("C:/Program Files (x86)/Mozilla Firefox/firefox.exe");

			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("moz:firefoxOptions", Options);
			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile ffProfile = allProfiles.getProfile("SeleniumProfile");
			capabilities.setCapability(FirefoxDriver.PROFILE, ffProfile);

			return new FirefoxDriver(capabilities);



		}
		else if ( browser.equalsIgnoreCase("chrome"))
		{  

			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			options.addArguments("disable-infobars");
			//options.addArguments("--user-data-dir");
			options.addArguments("--allow-running-insecure-content");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			return new ChromeDriver(capabilities);

		}
		else if (browser.equalsIgnoreCase("explorer"))
		{


			//System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/IEDriverServer.exe");
			DesiredCapabilities  dc = DesiredCapabilities.internetExplorer();
			dc.setJavascriptEnabled(true);
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			dc.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
			dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
			dc.setCapability(InternetExplorerDriver.IE_SWITCHES, true);
			dc.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			return new InternetExplorerDriver(dc);
		}
		else if(browser.equalsIgnoreCase("safari"))
		{

			return new SafariDriver();
		}
		else if(browser.equalsIgnoreCase("incognito"))
		{
			//System.setProperty("webdriver.chrome.driver",".\\src\\test\\java\\External_Jars\\libs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver","./src/test/java/External_Jars/lib/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-web-security");
			options.addArguments("--incognito");
			options.addArguments("--no-proxy-server");
			options.addArguments("disable-infobars");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			return new ChromeDriver(capabilities);			  
		}
		
		else if ( browser.equalsIgnoreCase("chrome_html"))
		{  

			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			options.addArguments("disable-infobars");
			//options.addArguments("--user-data-dir");
			options.addArguments("--allow-running-insecure-content");
			options.addArguments("--headless");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			return new ChromeDriver(capabilities);

		}
		
		else if ( browser.equalsIgnoreCase("html"))
		{  
			//System.setProperty("webdriver.htmlUnit.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\htmlunit-2.26.jar");
			HtmlUnitDriver options = new HtmlUnitDriver();
			//options.setJavascriptEnabled(true);
			/*//options.addArguments("--disable-web-security");
			//options.addArguments("--no-proxy-server");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			//options.setExperimentalOption("prefs", prefs);*/
			DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
			capabilities.setCapability(HtmlUnitDriver.INVALIDSELECTIONERROR, true);
			capabilities.setCapability(HtmlUnitDriver.INVALIDXPATHERROR, false);
			//capabilities.setCapability(, options);
			//capabilities.setJavascriptEnabled(true);
			//return new HtmlUnitDriver(BrowserVersion.CHROME ,true);
			return new HtmlUnitDriver();
		}
		
		else
		{

			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			return new ChromeDriver(capabilities);
		}
	}

	//Description: To verify if the element is enabled
	//Input = location of WebElement
	//Output = true if enabled, false if disabled

	public static boolean isEnabled(WebDriver driver, String LocatorType, String Element) throws IOException

	{
		boolean temp = false;
		try {
			String locator = GetPropertyFileData(driver ,Element);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				temp = driver.findElement(By.xpath(locator)).isEnabled();
			} else if (LocatorType.equalsIgnoreCase("css")) {

				temp = driver.findElement(By.cssSelector(locator)).isEnabled();
			} else if (LocatorType.equalsIgnoreCase("id")) {

				temp = driver.findElement(By.id(locator)).isEnabled();
			} else if (LocatorType.equalsIgnoreCase("class")) {

				temp = driver.findElement(By.className(locator)).isEnabled();
			} else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return temp;
	}

	//Description: To verify if the element is selected
	//Input = location of WebElement
	//Output = true if selected, false if not selected

	public static boolean isSelected(WebDriver driver, String LocatorType, String Element) throws IOException

	{
		boolean temp = false;
		try {
			String locator = GetPropertyFileData(driver ,Element);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				temp = driver.findElement(By.xpath(locator)).isSelected();
			} else if (LocatorType.equalsIgnoreCase("css")) {

				temp = driver.findElement(By.cssSelector(locator)).isSelected();
			} else if (LocatorType.equalsIgnoreCase("id")) {

				temp = driver.findElement(By.id(locator)).isSelected();
			} else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return temp;
	}


	//Description: To enter data into the TextBox
	//Input = value to be entered
	//Output = writes data into the TextBox

	public static void enterData(WebDriver driver, String TextboxName, String LocatorType, String Value)
			throws IOException {
		try {
			String locator = GetPropertyFileData(driver, TextboxName);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				//driver.findElement(By.xpath(locator)).clear();

				wait(driver);
				driver.findElement(By.xpath(locator)).sendKeys(Value);
			} 

			else if (LocatorType.equalsIgnoreCase("css")) {

				driver.findElement(By.cssSelector(locator)).clear();

				wait(driver);
				driver.findElement(By.cssSelector(locator)).sendKeys(Value);
			} 

			else if (LocatorType.equalsIgnoreCase("id")) {

				driver.findElement(By.id(locator)).clear();

				wait(driver);
				driver.findElement(By.id(locator)).sendKeys(Value);
			} 

			else if (LocatorType.equalsIgnoreCase("name")) {

				driver.findElement(By.name(locator)).clear();

				wait(driver);
				driver.findElement(By.name(locator)).sendKeys(Value);
			} 

			else if (LocatorType.equalsIgnoreCase("class")) {

				driver.findElement(By.name(locator)).clear();

				wait(driver);
				driver.findElement(By.name(locator)).sendKeys(Value);
			} 
			else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Description: To extract data 
	//Input = locator of WebElemet
	//Output = reads the text present in the WebElement
	public static String ExtractData(WebDriver driver, String TextboxName, String LocatorType) throws IOException
	{
		String temp = null;
		try {
			String locator = GetPropertyFileData(driver, TextboxName);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				temp = driver.findElement(By.xpath(locator)).getText();

			} else if (LocatorType.equalsIgnoreCase("css")) {

				temp = driver.findElement(By.cssSelector(locator)).getText();

			} else if (LocatorType.equalsIgnoreCase("id")) {

				temp = driver.findElement(By.id(locator)).getText();

			} else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return temp;
	}


	//Description: To read current Date and Time
	//Input = NA
	//Output = current Date and Time

	public static String currentDateTime()

	{
		Calendar currentDate = Calendar.getInstance(); // gets current date instance

		SimpleDateFormat formatter = new SimpleDateFormat("dd-mmm-yyyy HH:mm:ss");
		String dateNow = formatter.format(currentDate.getTime());
		return dateNow;
	}


	//Description: To select data in DropDown List by index
	//Input = locator of DropDown List  
	//Output = selects the particular option of DropDown List 

	public static void SelectDDLByIndex(WebDriver driver, String DropDownGroup, String LocatorType, int index)
			throws IOException, InterruptedException {
		try {
			String locator = GetPropertyFileData(driver, DropDownGroup);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				Select DDL = new Select(driver.findElement(By.xpath(locator)));

				wait(driver);
				DDL.selectByIndex(index);
			} else if (LocatorType.equalsIgnoreCase("css")) {

				Select DDL = new Select(driver.findElement(By.cssSelector(locator)));

				wait(driver);
				DDL.selectByIndex(index);
			} else if (LocatorType.equalsIgnoreCase("id")) {

				Select DDL = new Select(driver.findElement(By.id(locator)));

				wait(driver);
				DDL.selectByIndex(index);
			} else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	//Description: To select data in DropDown List by visible text
	//Input = locator of DropDown List  
	//Output = selects the particular option of DropDown List 

	public static void SelectDDLByVisibleText(WebDriver driver, String DropDownGroup, String LocatorType, String value)
			throws IOException, InterruptedException
	{

		try {
			String locator = GetPropertyFileData(driver, DropDownGroup);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				Select DDL = new Select(driver.findElement(By.xpath(locator)));

				wait(driver);
				DDL.selectByVisibleText(value);
			} else if (LocatorType.equalsIgnoreCase("css")) {

				Select DDL = new Select(driver.findElement(By.cssSelector(locator)));

				wait(driver);
				DDL.selectByVisibleText(value);
			} else if (LocatorType.equalsIgnoreCase("id")) {

				Select DDL = new Select(driver.findElement(By.id(locator)));

				wait(driver);
				DDL.selectByVisibleText(value);
			} else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	//Description: To accept the alert pop up
	//Input = alert pop-up 
	//Output = accepts alert

	public static void Alertaccept(WebDriver driver) throws IOException, InterruptedException
	{
		Alert A1 = driver.switchTo().alert();
		A1.accept();

		wait(driver);
	}


	//Description: To dismiss the alert pop up
	//Input = alert pop-up 
	//Output = dismisses alert

	public static void Alertdismiss(WebDriver driver) throws IOException, InterruptedException
	{
		WebElement Alert = driver.findElement(By.className("modal-content"));
		List<WebElement> buttons= Alert.findElements(By.tagName("button"));
		for ( WebElement ele1 : buttons)
		{
			if(ele1.getText().equalsIgnoreCase("no"))
			{
				ele1.click();
				break;
			}
		}

		//click(driver,"xpath" , "NoButton");

		wait(driver);
	}


	//Description: To select the check-box
	//Input = locator of check-box
	//Output = selected check-box

	public static void SelectCheckBox(WebDriver driver, String checkboxgroup, String LocatorType, String checkboxname)
			throws FileNotFoundException, IOException
	{
		List<WebElement> CheckBoxList = null;
		try {
			String locator = GetPropertyFileData(driver, checkboxgroup);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				CheckBoxList = driver.findElements(By.xpath(locator));

			} else if (LocatorType.equalsIgnoreCase("css")) {

				CheckBoxList = driver.findElements(By.cssSelector(locator));

			} else if (LocatorType.equalsIgnoreCase("id")) {

				CheckBoxList = driver.findElements(By.id(locator));

			} else {
				System.out.println("invalid selector");
			}

			for (int i = 0; i < CheckBoxList.size(); i++) {
				if (CheckBoxList.get(i).getAttribute("value").equals(checkboxname)) {
					CheckBoxList.get(i).click();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	//Description: To select the radio button
	//Input = locator of radio button
	//Output = selected radio button

	public static void SelectRadioButton(WebDriver driver, String Radiogroup, String LocatorType, String Radioname)
			throws FileNotFoundException, IOException 
	{
		List<WebElement> RadioList = null;
		try {
			String locator = GetPropertyFileData(driver, Radiogroup);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				RadioList = driver.findElements(By.xpath(locator));

			} else if (LocatorType.equalsIgnoreCase("css")) {

				RadioList = driver.findElements(By.cssSelector(locator));

			} else if (LocatorType.equalsIgnoreCase("id")) {

				RadioList = driver.findElements(By.id(locator));

			} else {
				System.out.println("invalid selector");
			}

			for (int i = 0; i < RadioList.size(); i++) {
				if (RadioList.get(i).getAttribute("value").equals(Radioname)) {
					RadioList.get(i).click();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	//Description: To click the WebElement
	//Input = locator of WebElement
	//Output = WebElement is clicked

	public static void click(WebDriver driver, String LocatorType, String buttonname) throws IOException 
	{
		try {
			String locator = GetPropertyFileData(driver, buttonname);

			if (LocatorType.equalsIgnoreCase("xpath"))
			{

				driver.findElement(By.xpath(locator)).click();
			}

			else if (LocatorType.equalsIgnoreCase("css")) 
			{

				driver.findElement(By.cssSelector(locator)).click();
			}

			else if (LocatorType.equalsIgnoreCase("id"))
			{

				driver.findElement(By.id(locator)).click();
			}
			else if (LocatorType.equalsIgnoreCase("class"))
			{

				driver.findElement(By.className(locator)).click();
			}
			else 
			{
				System.out.println("invalid selector");
			}

		} 

		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	//Description: To verify if the WebElement exists or not
	//Input = locator of WebElement
	//Output = true if exists, false if doesn't exist

	public static boolean Exist(WebDriver driver, String LocatorType, String Element) throws IOException

	{
		boolean temp = false;
		try {
			String locator = GetPropertyFileData(driver, Element);

			if (LocatorType.equalsIgnoreCase("xpath")) {

				temp = driver.findElement(By.xpath(locator)).isDisplayed();
			} else if (LocatorType.equalsIgnoreCase("css")) {

				temp = driver.findElement(By.cssSelector(locator)).isDisplayed();
			} else if (LocatorType.equalsIgnoreCase("id")) {

				temp = driver.findElement(By.id(locator)).isDisplayed();
			}else if (LocatorType.equalsIgnoreCase("class")) {

				temp = driver.findElement(By.className(locator)).isDisplayed();
			} else if (LocatorType.equalsIgnoreCase("link")) {

				temp = driver.findElement(By.linkText(locator)).isDisplayed();
			}
			else {
				System.out.println("invalid selector");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return temp;
	}

	//Description: To select date from calendar
	//Input = NA
	//Output = selects date from calendar

	public static void SelectDateFromCalender(WebDriver driver, String LocatorType, String Calender, String Date)
			throws IOException
	{
		int datetemp = 0;
		Boolean check = false;
		String locator = GetPropertyFileData(driver, Calender);
		WebElement dateWidget = driver.findElement(By.id(locator));
		List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.tagName("td"));
			for (WebElement cell : columns) {

				if (cell.getText().equals(Date)) {
					check = cell.findElement(By.linkText(Date)).isEnabled();
					if (check.equals(true)) {
						cell.findElement(By.linkText(Date)).click();
						datetemp = 1;
						break;
					}
				}

			}

			if (datetemp == 1) {
				break;
			}
		}

	}

	//Description: To close the browser

	public static void closeBrowser(WebDriver driver)
	{
		driver.close();

	}

	//Description: To verify the data in the table
	//Input = locator of table
	//Output = true if data exists in table else false

	public static Boolean verifyData(WebDriver driver, String tableloc, String element) throws FileNotFoundException, IOException 
	{
		String locator = GetPropertyFileData(driver, tableloc);
		WebElement Table = driver.findElement(By.xpath( locator));
		List<WebElement> options = Table.findElements(By.tagName("td"));

		for (int i = 0; i < options.size();i++) 
		{

			if (options.get(i).getText().equalsIgnoreCase(element))
			{

				return true;
			}

		}
		return false;
	}

	//Description: To click specific cell value in the table
	//Input = locator of table
	//Output = true if data exists in table else false
	public static void selectTableData(WebDriver driver, String tableloc, String element) throws FileNotFoundException, IOException, InterruptedException 
	{
		String locator = GetPropertyFileData(driver, tableloc);
		WebElement Table = driver.findElement(By.xpath( locator));
		List<WebElement> options = Table.findElements(By.tagName("td"));

		for (int i = 0; i < options.size();i++) 
		{

			if (options.get(i).getText().equalsIgnoreCase(element))
			{

				options.get(i).click();
			}

		}

		wait(driver);

	}

	//Description: To capture screenshot 
	//Input = name -> Folder name, Test -> TestName
	//Output = screenshot in the form of .png file

	public static void captureScreenshot (WebDriver driver , String name , String Test) throws IOException, InterruptedException 

	{

		wait(driver);
		File scrnsht = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);    
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-HH-mm-ss-SS");
		Date date = new Date();
		String Time = dateFormat.format(date);
		//FileUtils.copyFile(scrnsht, new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Results\\"+name +"\\"+ Test+"\\"+Time+"testing.png"));        
		FileUtils.copyFile(scrnsht, new File(System.getProperty("user.dir")+"/src/test/java/Test_Results/"+name +"/"+ Test+"/"+Time+"testing.png"));        

	}


	// Description: To traverse the table
	//Input = Locator of the table
	//Output = clicks on required element

	public static void traverseTable(WebDriver driver, String tablelocator, String element, String elementlocator)
			throws IOException, InterruptedException
	{

		try {
			String tableid = GetPropertyFileData(driver, tablelocator);
			WebElement Webtable = driver.findElement(By.xpath(tableid));
			List<WebElement> rows = Webtable.findElements(By.tagName("tr"));
			for (WebElement row : rows) {
				List<WebElement> columns = row.findElements(By.tagName("td"));
				for (WebElement cell : columns) {
					String coldata = cell.getText().toString();

					if (coldata.equalsIgnoreCase(element)) {

						wait(driver);
						cell.click();
						break;
					}
				}

			}

		} catch (Exception e) {

		}

	}


	//Description: To write the test result to excel sheet
	//Input = row and column of excel sheet
	//Output = writes data to excel sheet

	public static void Test_result( int sheetno ,int row ,int coloumn , String output ) throws IOException ,FileNotFoundException
	{
		int i = row ;
		//File file = new File(System.getProperty("user.dir")+"\\testOutputResult\\Automation Test Result.xlsx");
		File file = new File(System.getProperty("user.dir")+"/testOutputResult/Automation Test Result.xlsx");
		FileInputStream filestream = new FileInputStream (file);
		XSSFWorkbook workbook = new XSSFWorkbook(filestream);
		XSSFSheet sht = workbook.getSheetAt(sheetno);

		try
		{
			XSSFCell cell = sht.getRow(i).createCell(coloumn);
			cell.setCellValue(output);
			filestream.close();

			//FileOutputStream outFile =new FileOutputStream(new File(System.getProperty("user.dir")+"\\testOutputResult\\\\Automation Test Result.xlsx"));
			FileOutputStream outFile =new FileOutputStream(new File(System.getProperty("user.dir")+"/testOutputResult//Automation Test Result.xlsx"));
			workbook.write(outFile);
			outFile.close();


		}
		catch (Exception e)
		{
			e.printStackTrace();	
			System.out.println("file not found");
		}
		workbook.close();
	}

	//Description: To read the data from excel sheet
	//Input = Row and column of excel sheet
	//Output = Reads data from excel sheet

	public static String testdata( int sheetno ,int row ,int coloumn) throws IOException ,FileNotFoundException
	{
		int i = row ;
		String value = null ;
		File file = null;
		// String path = ExecutableJar.testdata1; //DND For Package file


		if( env.contains("stg"))
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataStg.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataStg.xlsx");
			//		 file = new File (path); //DND For Package file
		}
		else if(env.contains("PreProd"))
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataPreProd.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataPreProd.xlsx");
			//			 file = new File (path); //DND For Package file
		}
		else
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataProd.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataProd.xlsx");
			// file = new File (path); //DND For Package file
		}
		FileInputStream filestream = new FileInputStream (file);
		XSSFWorkbook workbook = new XSSFWorkbook(filestream);
		XSSFSheet sheet = workbook.getSheetAt(sheetno);

		try
		{
			XSSFCell cell = sheet.getRow(i).getCell(coloumn);
			value =  cell.toString();


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		workbook.close();  
		return value;
	}


	//Description: To find row count in excel sheet
	//Input = sheet no. of excel sheet
	//Output = count of rows in excel sheet

	public static int findrowscount( int sheetno ) throws IOException ,FileNotFoundException
	{
		int count = 0 ;
		File file1 = null;
		try 
		{
			if( env.contains("stg"))
			{
				//file1 = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataStg.xlsx");
				file1 = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataStg.xlsx");
			}
			else if(env.contains("PreProd"))
			{
				//file1 = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataPreProd.xlsx");
				file1 = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataPreProd.xlsx");
			}
			else
			{
				//file1 = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testDataProd.xlsx");
				file1 = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testDataProd.xlsx");
			}

			wait(driver);
			Workbook wb= WorkbookFactory.create(new FileInputStream(file1));		
			count = wb.getSheetAt(sheetno).getLastRowNum();
			wb.close();			
		}
		catch(Exception e)
		{
			System.out.println("The last row count has been failed and it is in catch field ");
			e.printStackTrace();
		}
		return count;		
	}


	/*
	 * the ChangeDownloadLocation method will change the location of download file to D drive 
	 * this method is applicable for the chrome and firefox browser.
	 */
	public static void ChangeDownloadLocation(WebDriver driver, String browser) throws InterruptedException
	{
		if(browser.equalsIgnoreCase("firefox"))
		{
			driver.get("about:preferences");

			wait(driver);
			try {
				Robot robot = new Robot();

				wait(driver);
				robot.mouseWheel(20);

				wait(driver);
			} 
			catch (AWTException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			driver.findElement(By.id("chooseFolder")).click();
			try {
				//Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\DownloadChangeLoc.exe");
				Runtime.getRuntime().exec(System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/DownloadChangeLoc.exe");
				
				wait(driver);
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		else if(browser.equalsIgnoreCase("chrome")) 
		{
			driver.get("chrome://settings/?search=change");
			Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_TAB);

				wait(driver);
				robot.keyPress(KeyEvent.VK_TAB);

				wait(driver);
				robot.keyPress(KeyEvent.VK_ENTER);

				wait(driver);
			} catch (AWTException e1) {
				e1.printStackTrace();}
			try {
				//Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\ChromeChangeLocDownload.exe");
				Runtime.getRuntime().exec(System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/ChromeChangeLocDownload.exe");

				wait(driver);
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}

		// for any browser
		else 
		{
			driver.get("chrome://settings/?search=change");
			Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_TAB);

				wait(driver);
				robot.keyPress(KeyEvent.VK_TAB);

				wait(driver);
				robot.keyPress(KeyEvent.VK_ENTER);

				wait(driver);
			} catch (AWTException e1) {
				e1.printStackTrace();}
			try {
				//Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\test\\java\\External_Jars\\libs\\ChromeChangeLocDownload.exe");
				Runtime.getRuntime().exec(System.getProperty("user.dir")+"/src/test/java/External_Jars/libs/ChromeChangeLocDownload.exe");

				wait(driver);
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}

	}

	//Description: To write the result in the excel sheet
	//Input = NA
	//Output = pass if passed, fail if failed
	public static void testReport(int rowCount,String result) throws FileNotFoundException, IOException
	{
		if( env.contains("stg"))
		{
			BaseClass.Test_result(0, rowCount,5, result);
			System.out.println("The Result is successfully written in the excel sheet");
		}
		else if(env.contains("PreProd"))
		{
			BaseClass.Test_result(2, rowCount,5, result);
			System.out.println("The Result is successfully written in the PreProd Excel Sheet");
		}
		else {
			BaseClass.Test_result(1, rowCount,5, result);
			System.out.println("The Result is successfully written in the excel sheet");
		}

	}

	//Description: To write the time of the report
	//Input = NA
	//Output = time
	public static void testTimeReport(int rowCount,long startTime,long endTime) throws FileNotFoundException, IOException
	{
		long executionTime = endTime-startTime;
		String execTime = Long.toString(executionTime);
		if( env.contains("stg"))
		{
			BaseClass.Test_result(0, rowCount,6, execTime);
			System.out.println("The Result is successfully written in the Stg Excel Sheet");
		}
		else if(env.contains("PreProd")) 
		{
			BaseClass.Test_result(2, rowCount,6, execTime);
			System.out.println("The Result is successfully written in the PreProd Excel Sheet");
		}
		else {
			BaseClass.Test_result(1, rowCount,6, execTime);
			System.out.println("The Result is successfully written in the Prod Excel Sheet");
		}

	}

	//Description: To write the test result to excel sheet
	//Input = row and column of excel sheet
	//Output = writes data to excel sheet

	public static void WriteIntoExcelSheet( int sheetno ,int row ,int coloumn , String output ) throws IOException ,FileNotFoundException
	{

		int i = row ;
		File file = null;
		FileOutputStream outFile=null;
		if( env.contains("stg"))
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataStg.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testInputDataStg.xlsx");
		}
		else if(env.contains("PreProd")) 
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataPreProd.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testInputDataPreProd.xlsx");
		}
		else
		{
			//file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataProd.xlsx");
			file = new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testInputDataProd.xlsx");
		}

		FileInputStream filestream = new FileInputStream (file);
		XSSFWorkbook workbook = new XSSFWorkbook(filestream);
		XSSFSheet sht = workbook.getSheetAt(sheetno);

		try
		{
			XSSFCell cell = sht.getRow(i).createCell(coloumn);
			cell.setCellValue(output);
			filestream.close();

			if( env.contains("stg"))
			{
				//outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataStg.xlsx"));
				outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testInputDataStg.xlsx"));
			}
			else if(env.contains("PreProd")) 
			{
				//outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataPreProd.xlsx"));
				outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"/src/test/java/Test_Data/testInputDataPreProd.xlsx"));
			}
			else
			{
				//outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"\\src\\test\\java\\Test_Data\\testInputDataProd.xlsx"));
				outFile=new FileOutputStream(new File(System.getProperty("user.dir")+"/src\\test/java/Test_Data/testInputDataProd.xlsx"));
			}

			workbook.write(outFile);
			outFile.close();


		}
		catch (Exception e)
		{
			e.printStackTrace();	
			System.out.println("file not found");
		}
		workbook.close();	
	}


}
